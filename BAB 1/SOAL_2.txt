Nama    : Diani Eka Putri
NIM     : 1207050027
Kelas	: Teknik Informatika-B
Kelompok: 3

Soal Latihan Bab 1
2.  Tiga pasang suami istri yang sedang menempuh perjalanan sampai ke  sebuah sungai.
    Di situ mereka menemukan sebuah perahu kecil yang hanya bisa membawa tidak lebih 
    dari dua orang setiap kali menyeberang. Penyeberangan sungai dirumitkan oleh 
    kenyataan bahwa para suami sangat pencemburu dan tidak mau meninggalkan istri-istri mereka jika ada lelaki lain.
    Tulislah algoritma dalam notasi deskriptif saja untuk menunjukkan bagaimana penyeberangan itu bisa dilakukan.
     
Jawab
Misal tiga pasangan suami istri tersebut dengan  :
1. L₁ dan P₁
2. L₂ dan P₂
3. L₃ dan P₃  
Dengan :
• L = laki-laki/suami
• P = Perempuan/Istri
Karena suaminya pencemburu maka seorang istri hanya boleh menyebrang dengan istri yang lain atau dengan suaminya sendiri.
Algoritma penyebrangan 3 pasang suami istri dimana ketiga suami memiliki sifat cemburu.
1. Start.
2. L₁ dan L₂ naik perahu dan menyebrang ke sisi lain sungai.
3. L₁ turun, L₂ kembali lagi ke tempat semula.
4. Setelah di tempat semula, pasangan L₂ dan P₂ naik perahu kemudian mereka berdua turun.
5. Setelah sampai di sebrang L₁ naik perahu untuk kembali lagi ke tempat semula.
6. L₁ dan L₃ naik perahu.
7. L₁ turun dan L₃ kembali lagi ke tempat semula untuk menjemput istrinya P₃.
8. Setelah di tempat semula, pasangan L₃ dan P₃ naik perahu kemudian mereka berdua turun.
9. Setelah sampai di sebrang L₁ naik kembali untuk menjemput istrinya P₁ di tempat semula.
10. Pasangan L₁ dan P₁ naik perahu dan turun di sebrang sungai.
11. Dan akhirnya mereka pun dapat menyebrang semua tanpa adanya cemburu.
12. Selesai.
