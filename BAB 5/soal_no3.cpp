
#include <iostream>
#define PHI 3.14

using namespace std;

int main()
{
	
  float k, r;
  
  cout << "=====================================\n";
  cout << "Program Menghitung Keliling Lingkaran\n";
  cout << "=====================================\n";
  cout << "Masukkan panjang jari-jari lingkaran: ";
  cin >> r;
  k = 2 * 3.14 * r;
  cout << "Keliling Lingkaran adalah " << k << endl;
  
  return 0;
}
