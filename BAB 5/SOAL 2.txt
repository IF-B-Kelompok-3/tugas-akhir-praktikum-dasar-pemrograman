Nama 	: Frinaldi Muhammad Syauqi
NIM	: 1207050041
Kelas	: Teknik Informatika-B
Kelompok: 3

Soal Latihan Bab 5:
2.	Translasilah algoritma pada Latihan Bab 4 ke dalam program Pascal, C, atau C++ 

Jawaban: 
-Program 1 :

#include <iostream> //Program Sapaan
#include <string>
using namespace std;

main() {
    string b,d;
    cout<<"Halo, siapa namamu? ";
    getline(cin,b);
    cout<<"Di kota apa kamu sekarang? ";
    getline(cin,d);
    cout<<"Senang bertemu denganmu "<<b<< ", di kota "<<d;
}

-Program 2 :

#include <iostream> //Program Menghitung Luas Bidang Geometri Lain
using namespace std;

main() { 
    float a,t,L;
	cout << "Masukkan Alas (cm)    : ";
	cin >> a;
	cout << "Masukkan Tinggi (cm)  : ";
	cin >> t;
	L = a*t;
	cout << "Luas Jajar Genjang    : "<<L<<"cm^2";
}

-Program 3 :

#include <iostream> //Program Konversi Suhu C ke F
using namespace std;

main() { 
  float c,f;
  cout << "Masukkan Suhu Celsius : ";
  cin >> c;
  f= (c * 9 / 5) + 32;
  cout << "Fahrenheit : " << f << " F";
}

-Program 4 :

#include <iostream> //Program Konversi Suhu F ke C
using namespace std;

main() { 
  float c,f;
  cout << "Masukkan Suhu Fahrenheit : ";
  cin >> f;
  c = (f - 32) * 5/9;
  cout << "Celsius : " << c << " C";
}

-Program 5 :

#include <iostream> //Program Menghitung Jarak
using namespace std;

main() { 
    float v,s,t;
    cout<<"Masukan Kecepatan (Km/jam) : ";
    cin>>v;
    cout<<"Masukan Waktu Tempuh (Jam) : ";
    cin>>t;
    s=v*t;
    cout<<"Jarak (Km)                 : "<<s;
}

-Program 6 :

#include <iostream> //Program Menghitung Luas Segitiga
using namespace std;

main() {
    float a,t,L;
	cout << "Masukkan Alas (cm)    : ";
	cin >> a;
	cout << "Masukkan Tinggi (cm)  : ";
	cin >> t;
	L = (a*t)/2;
	cout << "Luas Segitiga         : "<<L<<"cm^2";
}

-Program 7 :

#include <iostream> //Program Menghitung Diskon
using namespace std;

main() {
  float harga,diskon,potongan,total;
  cout<<"Masukkan Jumlah Harga : Rp.";
  cin>>harga;
  cout<<"Masukkan Diskon (%)   : ";
  cin>>diskon;
  potongan = (diskon/100)*harga;
  total = harga-potongan;
  cout<<"\nHarga Asli            : Rp."<<harga<<endl;
  cout<<"Potongan Diskon       : Rp."<<potongan<<endl;
  cout<<"Harga Setelah Diskon  : Rp."<<total;
}

-Program 8 :

#include <iostream> //Program Menghitung Bunga
using namespace std;

main() {
    int m,n,r;
    cout<<"Uang Tabungan                 : Rp.";
    cin>>m;
    cout<<"Bunga/Tahun (%)               : ";
    cin>>r;
    cout<<"\nTotal Bunga Dalam Setahun     : Rp."<<r*m/100<<endl;
    cout<<"Jumlah Tabungan Dalam Setahun : Rp."<<m+r*m/100;
}