#include <iostream>
using namespace std;

int main(){
	 
	 float makan, diskon, pajak, bayar;
	 
	 cout << "\t===Menghitung Total Pembayaran===\n" << endl;
	 cout << "Total Harga Makan Restoran	: ";
	 cin >> makan; 
	 
	 if(makan>=200000)
	 {
	 	diskon=0.15*makan; 
	 }
	 else if(makan>=100000)
	 {
	 	diskon=0.05*makan; 
	 }
	 else
	 {
	 	diskon=0;
	 }
	 
	 pajak=makan*0.1; 
	 bayar=makan+pajak-diskon; 
	 
	 cout << "Anda mendapat diskon sebesar	: " << diskon << endl; 
	 cout << "Pajak 10% dari total makan	: " << pajak << endl; 
	 cout << "\n\nMaka Anda harus membayar	: " << bayar << endl; 
	 cout << "\t\n=========================================" << endl;
	 	 
	return 0;
}

